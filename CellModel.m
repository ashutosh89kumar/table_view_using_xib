//
//  cellModel.m
//  tableViewUsingXib
//
//  Created by CRATERZONE on 22/03/15.
//  Copyright (c) 2015 CRATERZONE. All rights reserved.
//

#import "CellModel.h"

@interface CellModel ()

@end

@implementation CellModel
@synthesize particularCellDetail;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)initializeDetail {
    particularCellDetail = [NSMutableArray arrayWithObjects:
                                        [NSMutableArray arrayWithObjects:
                                                [NSMutableArray arrayWithObjects:@"Airplane mode",@"AirplaneMode.png",@"NO", nil],
                                                [NSMutableArray arrayWithObjects:@"Wi-Fi",@"Wifi.png",@"YES", nil],
                                                [NSMutableArray arrayWithObjects:@"Bluetooth",@"Bluetooth.png",@"YES", nil]
                                         , nil],
                                        [NSMutableArray arrayWithObjects:
                                                [NSMutableArray arrayWithObjects:@"Notifications",@"Notifications.png",@"YES", nil],
                                                [NSMutableArray arrayWithObjects:@"Control Center",@"ControlCenter.png",@"YES", nil],
                                                [NSMutableArray arrayWithObjects:@"Do Not Disturb",@"DoNotDisturb.png",@"YES", nil]
                                         , nil],
                                        [NSMutableArray arrayWithObjects:
                                                [NSMutableArray arrayWithObjects:@"General",@"General.png",@"YES", nil],
                                                [NSMutableArray arrayWithObjects:@"Display & Brightness",@"Display&Brightness.png",@"YES", nil],
                                                [NSMutableArray arrayWithObjects:@"Wallpaper",@"Wallpaper.png",@"YES", nil],
                                                [NSMutableArray arrayWithObjects:@"Sounds",@"Sounds.png",@"YES", nil],
                                                [NSMutableArray arrayWithObjects:@"Passcode",@"Passcode.png",@"YES", nil],
                                                [NSMutableArray arrayWithObjects:@"Privacy",@"Privacy.png",@"YES", nil]
                                         , nil],
                                        [NSMutableArray arrayWithObjects:
                                                [NSMutableArray arrayWithObjects:@"iCloud",@"iCloud.png",@"YES", nil],
                                                [NSMutableArray arrayWithObjects:@"iTunes & App Store",@"iTunes&AppStore.png",@"YES", nil]
                                         , nil],
                                        [NSMutableArray arrayWithObjects:
                                                [NSMutableArray arrayWithObjects:@"Mail, Contacts, Calenders",@"Mail,Contacts,Calendars.png",@"YES", nil],
                                                [NSMutableArray arrayWithObjects:@"Notes" , @"Notes.png",@"YES", nil],
                                                [NSMutableArray arrayWithObjects:@"Reminders",@"Reminders.png",@"YES", nil],
                                                [NSMutableArray arrayWithObjects:@"Messages",@"Messages.png",@"YES", nil],
                                                [NSMutableArray arrayWithObjects:@"FaceTime",@"FaceTime.png",@"YES", nil],
                                                [NSMutableArray arrayWithObjects:@"Maps",@"Maps.png",@"YES", nil],
                                                [NSMutableArray arrayWithObjects:@"Safari",@"Safari.png",@"YES", nil]
                                         , nil],
                                        [NSMutableArray arrayWithObjects:
                                                [NSMutableArray arrayWithObjects:@"Music",@"Music.png",@"YES", nil],
                                                [NSMutableArray arrayWithObjects:@"Videos",@"Videos.png",@"YES", nil],
                                                [NSMutableArray arrayWithObjects:@"Photo & camera",@"Photos&Camera.png",@"YES", nil],
                                                [NSMutableArray arrayWithObjects:@"iBooks",@"iBooks.png",@"YES", nil],
                                                [NSMutableArray arrayWithObjects:@"Podcasts",@"Pdcasts.png",@"YES", nil],
                                                [NSMutableArray arrayWithObjects:@"iTunes U",@"iTunesU.png",@"YES", nil],
                                                [NSMutableArray arrayWithObjects:@"Game Center",@"GameCenter.png",@"YES", nil]
                                         , nil],
                                        [NSMutableArray arrayWithObjects:
                                                [NSMutableArray arrayWithObjects:@"Twitter", @"Twitter.png", @"YES", nil],
                                                [NSMutableArray arrayWithObjects:@"Facebook", @"Facebook.png", @"YES", nil],
                                                [NSMutableArray arrayWithObjects:@"Flickr", @"Flickr.png", @"YES", nil],
                                                [NSMutableArray arrayWithObjects:@"Vimeo", @"Vimeo.png", @"YES", nil]
                                        , nil],
                                        [NSMutableArray arrayWithObjects:
                                                [NSMutableArray arrayWithObjects:@"GarageBand", @"GarageBand.png", @"YES", nil],
                                                [NSMutableArray arrayWithObjects:@"iMovie", @"iMovie.png", @"YES", nil],
                                                [NSMutableArray arrayWithObjects:@"Keynotes", @"Keynote.png", @"YES", nil],
                                                [NSMutableArray arrayWithObjects:@"nike + iPods", @"Nike+iPod.png", @"YES", nil],
                                                [NSMutableArray arrayWithObjects:@"Numbers", @"Numbers.png", @"YES", nil],
                                                [NSMutableArray arrayWithObjects:@"Pages", @"Pages.png", @"YES", nil]
                                        , nil]
                            
                            
                            , nil];
}

#pragma mark - Navigation
/*
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
