//
//  AppDelegate.h
//  tableViewUsingXib
//
//  Created by CRATERZONE on 20/03/15.
//  Copyright (c) 2015 CRATERZONE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) ViewController *tableView;
@property (strong, nonatomic) UINavigationController *navController;


@end

