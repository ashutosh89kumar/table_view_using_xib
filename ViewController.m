//
//  SecondTableView.m
//  tableViewUsingXib
//
//  Created by CRATERZONE on 20/03/15.
//  Copyright (c) 2015 CRATERZONE. All rights reserved.
//

#import "ViewController.h"
#import "CustomTableCell.h"
#import "BluetoothPage.h"
#import "CellModel.h"

@interface ViewController ()//<UITableViewDataSource,UITableViewDelegate>

@end
CellModel *cellModelObject;

@implementation ViewController

- (void)viewDidLoad {

    [super viewDidLoad];
    [self addEditAndAddButtonInNavigationBar];
    cellModelObject=[[CellModel alloc]init];
    [cellModelObject initializeDetail];
    [self setTitle:@"Setting"];


}

-(void) addEditAndAddButtonInNavigationBar {
    editButton = [[UIBarButtonItem alloc]initWithTitle:@"Edit"
                                                                  style:UIBarButtonItemStyleDone
                                                                 target:self
                                                                 action:@selector(edit:)];
    [editButton setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:[UIColor blackColor],NSForegroundColorAttributeName,nil]
                              forState:UIControlStateNormal];
    editButton.tag = 0;
    editButton.possibleTitles = [NSSet setWithObjects:@"Edit", @"Done", nil];
    self.navigationItem.leftBarButtonItem=editButton;
    
    addButton = [[UIBarButtonItem alloc]initWithTitle:@"Add"
                                                style:UIBarButtonItemStyleDone
                                               target:self
                                               action:@selector(add:)];
    addButton.tag = 0;
    addButton.possibleTitles = [NSSet setWithObjects:@"Add",@"Done", nil];
    self.navigationItem.rightBarButtonItem=addButton;
    [addButton setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:[UIColor blackColor],NSForegroundColorAttributeName,nil]
                              forState:UIControlStateNormal];
}

-(void) edit:(UIBarButtonItem*) barBtnItem {
    if (barBtnItem.tag == 0)
    {
        barBtnItem.tag = 1;
        [editButton setTitle:@"Done"];
        [editButton setTintColor:[UIColor blueColor]];
        [editButton setTitleTextAttributes:
         [NSDictionary dictionaryWithObjectsAndKeys:[UIColor blueColor],NSForegroundColorAttributeName,nil]
                                  forState:UIControlStateNormal];
        [self setEditing:YES animated:YES];
        [self.settingTableView setEditing:YES animated:YES];
    }
    else
    {
        barBtnItem.tag = 0;
        [editButton setTitle:@"Edit"];
        [self setEditing:YES animated:YES];
        [self.settingTableView setEditing:NO animated:YES];
        [editButton setTitleTextAttributes:
         [NSDictionary dictionaryWithObjectsAndKeys:[UIColor blackColor],NSForegroundColorAttributeName,nil]
                                  forState:UIControlStateNormal];
    }
    
}

-(void) add:(UIBarButtonItem*) barBtnItem {
    //UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(10, 200, 300, 40)];
    if (barBtnItem.tag == 0) //add
    {
        barBtnItem.tag = 1;
        [addButton setTitle:@"Done"];
        [addButton setTintColor:[UIColor blueColor]];
        [addButton setTitleTextAttributes:
         [NSDictionary dictionaryWithObjectsAndKeys:[UIColor blueColor],NSForegroundColorAttributeName,nil]
                                  forState:UIControlStateNormal];
        NSMutableArray *newData=[NSMutableArray arrayWithObjects:@"New Cell",@"search.jpg",@"YES", nil];
        int section=[cellModelObject.particularCellDetail count];
        int row=[[cellModelObject.particularCellDetail objectAtIndex:(section-1)]count];
        [self.settingTableView setContentOffset:CGPointMake(0,(section*row)*38)];
        //UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(10, 200, 300, 40)];
//        textField.borderStyle = UITextBorderStyleRoundedRect;
//        textField.font = [UIFont systemFontOfSize:15];
//        textField.placeholder = @"enter text";
//        textField.autocorrectionType = UITextAutocorrectionTypeNo;
//        textField.keyboardType = UIKeyboardTypeDefault;
//        textField.returnKeyType = UIReturnKeyDone;
//        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
//        textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
//        textField.delegate = self;
//        [self.view addSubview:textField];
        //[textField release];
        
        
        
        [[cellModelObject.particularCellDetail objectAtIndex:(section-1)]insertObject:newData atIndex:(row-1)];
    }
    else  //done
    {
        //textField=nil;
        barBtnItem.tag = 0;
        [addButton setTitle:@"Add"];
        [addButton setTitleTextAttributes:
         [NSDictionary dictionaryWithObjectsAndKeys:[UIColor blackColor],NSForegroundColorAttributeName,nil]
                                  forState:UIControlStateNormal];
    }
    
}
// Override to support conditional editing of the table view.
// This only needs to be implemented if you are going to be returning NO
// for some items. By default, all items are editable.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

//function to delete particular cell

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [[cellModelObject.particularCellDetail objectAtIndex:indexPath.section] removeObjectAtIndex:indexPath.row];
        
        [tableView reloadData];
    }
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return [cellModelObject.particularCellDetail count];
    //return 3;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    //return [cellDataArray count];
    return [[cellModelObject.particularCellDetail objectAtIndex:section] count];
    //return 10;
}


// Setting Row Height For TableView
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier=@"cellIdentifier";
    CustomTableCell *cell= [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        //cell = [[CustomTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        NSArray *nib=[[NSBundle mainBundle] loadNibNamed:@"CustomTableCell" owner:self options:nil];
        cell=[nib objectAtIndex:0];
        
    }
    
    int row=(int)indexPath.row;
    int section=(int)indexPath.section;

    cell.cellTitleLabel.text=[[[cellModelObject.particularCellDetail objectAtIndex:section] objectAtIndex:row]objectAtIndex:0];
    cell.cellAvtarImage.image=[UIImage imageNamed:[[[cellModelObject.particularCellDetail objectAtIndex:section] objectAtIndex:row]objectAtIndex:1]];
    if(![self getHiddenStatus:section withColumNo:row])
    {
            cell.ceellSwitchButton.hidden=[self getHiddenStatus:section withColumNo:row];
            [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    else{
        cell.ceellSwitchButton.hidden=YES;
    }
    
    
    return cell;
}



-(BOOL) getHiddenStatus:(int)section withColumNo:(int)row {
    if([[[[cellModelObject.particularCellDetail objectAtIndex:section] objectAtIndex:row]objectAtIndex:2] isEqualToString:@"YES"])
        return YES;
    return NO;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    if([[[[cellModelObject.particularCellDetail objectAtIndex:indexPath.section ] objectAtIndex:indexPath.row]objectAtIndex:0 ]isEqualToString:@"Bluetooth"])
    {   BluetoothPage *bluetoothPageObj=[[BluetoothPage alloc]init];
        [self.navigationController pushViewController:bluetoothPageObj animated:YES];
    }
    //[[[[cellModelObject.particularCellDetail objectAtIndex:indexPath.section ] objectAtIndex:indexPath.row]objectAtIndex:0 ]isEqualToString:@"Bluetooth"]
}


@end
