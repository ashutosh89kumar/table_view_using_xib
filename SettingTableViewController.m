//
//  SettingTableViewController.m
//  tableViewUsingXib
//
//  Created by CRATERZONE on 21/03/15.
//  Copyright (c) 2015 CRATERZONE. All rights reserved.
//

#import "SettingTableViewController.h"

@interface SettingTableViewController ()



@end

@implementation SettingTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier=@"cellIdentifier";
    UITableView  *cell= [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        //cell = [[UITableView alloc] initWithStyle :UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell= [[UITableView alloc] initWithCoder:<#(NSCoder *)#>]
//        NSArray *nib=[[NSBundle mainBundle] loadNibNamed:@"CustomTableCell" owner:self options:nil];
//        cell=[nib objectAtIndex:0];
        
    }
    
    
    
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
