//
//  cellModel.h
//  tableViewUsingXib
//
//  Created by CRATERZONE on 22/03/15.
//  Copyright (c) 2015 CRATERZONE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellModel : UIViewController

@property NSMutableArray *particularCellDetail;
-(void)initializeDetail ;
@end

