//
//  BluetoothPage.h
//  tableViewUsingXib
//
//  Created by CRATERZONE on 22/03/15.
//  Copyright (c) 2015 CRATERZONE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BluetoothPage : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *bluetoothLabel;
@property (strong, nonatomic) IBOutlet UILabel *bluetoothDeviceName;
@property (strong, nonatomic) IBOutlet UILabel *devices;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *searchingProgressBar;
@property (strong, nonatomic) IBOutlet UILabel *bluetoothDevie1;
@property (strong, nonatomic) IBOutlet UILabel *bluetoothDevice2;
//@property(nonatomic,readonly,retain) UINavigationItem *navigationItem; // Created on-demand so that a view controller may customize its navigation appearance.

@end
