//
//  CustomTableCell.h
//  tableViewUsingXib
//
//  Created by CRATERZONE on 20/03/15.
//  Copyright (c) 2015 CRATERZONE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTableCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *cellTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *cellButtonLabel;

@property (strong, nonatomic) IBOutlet UIButton *cellButton;

@property (strong, nonatomic) IBOutlet UIImageView *cellAvtarImage;
@property (strong, nonatomic) IBOutlet UIView *cellComponent;
@property (strong, nonatomic) IBOutlet UISwitch *ceellSwitchButton;


@end
