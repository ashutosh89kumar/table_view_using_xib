//
//  SettingTableViewController.h
//  tableViewUsingXib
//
//  Created by CRATERZONE on 21/03/15.
//  Copyright (c) 2015 CRATERZONE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingTableViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *SettingTableView;

@end
