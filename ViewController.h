//
//  SecondTableView.h
//  tableViewUsingXib
//
//  Created by CRATERZONE on 20/03/15.
//  Copyright (c) 2015 CRATERZONE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
{
    NSArray *particularCellDetail;
    UIBarButtonItem *editButton,*addButton;
}
@property (strong, nonatomic) IBOutlet UITableView *settingTableView;

@end
