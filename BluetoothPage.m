//
//  BluetoothPage.m
//  tableViewUsingXib
//
//  Created by CRATERZONE on 22/03/15.
//  Copyright (c) 2015 CRATERZONE. All rights reserved.
//

#import "BluetoothPage.h"

@interface BluetoothPage ()

@end

@implementation BluetoothPage
//@synthesize navigationItem;
@synthesize bluetoothLabel=_bluetoothLabel;
@synthesize bluetoothDeviceName=_bluetoothDeviceName;
@synthesize devices=_devices;
@synthesize searchingProgressBar=_searchingProgressBar;
@synthesize bluetoothDevie1=_bluetoothDevie1;
@synthesize bluetoothDevice2=_bluetoothDevie2;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"Bluetooth"];
    //[self _navigationItem.title=@"Bluetooth"];
    //self.navigationItem.title=@"Bluetooth";
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
